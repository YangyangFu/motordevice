within MotorDevice.BaseClasses;
partial model PartialMechanic

  Modelica.SIunits.Angle phi "Shaft angle";
  Modelica.SIunits.AngularVelocity omega "Shaft angular velocity";
  Real Nrpm "Rational speed";

  Modelica.Mechanics.Rotational.Interfaces.Flange_b shaft
  annotation (Placement(transformation(extent={{-10,90},{10,110}})));

equation
  phi = shaft.phi;
  omega = der(phi);
  Nrpm = Modelica.SIunits.Conversions.to_rpm(omega);

  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)));
end PartialMechanic;
